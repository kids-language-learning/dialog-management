import boto3
import botocore
import logging
import requests
import json

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

TABLE_NAME = 'LanguageUsers'
dynamodb = boto3.resource('dynamodb')
TABLE = dynamodb.Table(TABLE_NAME)

KEY_RESPONSE_TEXT = "responseText"
word_list=[
    {"en": "duck", "es": "el pato", "action": "quack", "audio_url": "https://www.ichime.com/s/5-10.mp3"},
    {"en": "rabbit", "es": "el conejo", "action": "hopp", "audio_url": ""},
    {"en": "sheep", "es": "la oveja", "action": "swim", "audio_url":"https://kids-language-learning.s3.amazonaws.com/sentence/sheep_sound.mp3"},
    {"en": "horse", "es": "el caballo", "action": "run", "audio_url":"https://free-screensavers-backgrounds.com/ringtones/funny/horse.mp3"},
    {"en": "fish", "es": "el pez", "action": "swim", "audio_url":""},
    {"en": "dog", "es": "el perro", "action": "run", "audio_url":"https://www.eletech.com/Products/Kiddie_Ride_Boards/Dog.wav"}
]

session_attributes = {}


class State:
    NONE = 'NONE'
    START = 'START'
    TEACH_NEW_WORD = 'TEACH_NEW_WORD'
    REPEAT_WORD = 'REPEAT_WORD'
    # TEACH_NEW_WORD_ANS_CORRECT = 'TEACH_NEW_WORD_ANS_CORRECT'
    # TEACH_NEW_WORD_ANS_WRONG = 'TEACH_NEW_WORD_ANS_WRONG'
    POST_TEST = 'POST_TEST'


def handler(event, context):
    logger.info("Handle lambda event: {}".format(event))

    intent = event['intent']
    logger.info("intent: {}".format(intent))

    initialize_session_attributes(event['parameters']['session_id'])

    if not intent:
        return {KEY_RESPONSE_TEXT: "Sorry, I don't understand."}


    if intent == "LaunchIntent":
        response = handle_launch_intent(event)
    elif intent == "repeat":
        response = handle_repeat_request(event)
    elif session_attributes['last_state'] == State.START:
        response = handle_first_game(event)
    elif session_attributes['last_state'] == State.TEACH_NEW_WORD:
        response = handle_new_word_answer(event)
    elif session_attributes['last_state'] == State.REPEAT_WORD:
        response = handle_repeat_word_answer(event)

    # elif intent == "fruit-english":
    #     response = handle_fruit_english_intent(event)
    # elif intent == "fruit-spanish":
    #     response = handle_fruit_spanish_intent(event)
    else:
        response = get_default_response()

    put_session_attribute_to_table()

    return response

def get_default_response():
    ssml = ssml_speak('''Sorry, I didn't get that. Can you say it again?''')
    audio_uri = get_tts(ssml)

    return {
        "word": None,
        "responseText": "",
        "audio_uri": audio_uri,
        "display": {
            "text": "",
            "images": [
                {
                    "key": "launch",
                    "uri": "uri"
                }
            ]
        },
        "feedback":""
    }
# session attributes

def initialize_session_attributes(session_id):
    global session_attributes
    item = get_table_item(session_id)
    if item is None:
        session_attributes['session_id'] = session_id
        session_attributes['last_state'] = None
        session_attributes['last_word_id'] = -1
        session_attributes['last_answer_is_correct'] = True

    else:
        session_attributes = item

    print("session_attributes: {}".format(session_attributes))

def get_state():
    return session_attributes['state']

def set_state(state):
    session_attributes['state'] = state

def set_last_word_id(word_id):
    session_attributes['last_word_id'] = word_id

def get_last_word_id():
    return int(session_attributes['last_word_id'])

def get_last_word():
    return word_list[get_last_word_id()]

def handle_launch_intent(event):    
    logger.info("handle launch event")

    ssml = ssml_speak('''Welcome to the Spanish zoo! I'm the zoo keeper. Let me show you the animals I have. Let's see how good you are at speaking their Spanish name! Are you ready?''')
    
    audio_uri = get_tts(ssml, "intro")

    session_attributes['last_state'] = State.START

    return {
        "responseText": "Welcome to the Spanish zoo! Let me show you the animals I have. Let's see how good you are at speaking their Spanish name!",
        "audio_uri": audio_uri,
        # "audio_uri": "intro",
        "display": {
            "text": "",
            "images": [
                {
                    "key": "launch",
                    "uri": "uri"
                }
            ]

        }
    }

def handle_first_game(event):
    logger.info("handle first game")

    word_id = get_last_word_id() + 1

    acknowledgement = "Great!"


    return teach_new_word(event, acknowledgement, True, word_id)
    

def teach_new_word(event, acknowledgement, is_last_answer_correct, word_id, reward_images=None):
    session_attributes['error_count'] = 0

    word = word_list[word_id]

    # ssml = ssml_speak(''' Great! {ssml_break} {teach_sentence}'''.format(
    #     ssml_break=ssml_break(),
    #     teach_sentence=get_teaching_sentence(word)
    # ))
    logger.info(word)

    ssml = ssml_speak('''{acknowledgement} This is our {animal}. {ssml_break} {teach_sentence} {ssml_break} <s>Can you repeat with me?</s> You can click the {animal} to listen again.'''.format(
        acknowledgement=acknowledgement, 
        animal=word['en'], 
        teach_sentence=get_teaching_sentence(word), 
        ssml_break=ssml_break(),
        spanish_word=ssml_spanish(word['es'])))

    logger.info("ssml: {}".format(ssml))

    audio_file_name = get_audio_file_name_for_teach_new_word(is_last_answer_correct, word['en'])
    audio_uri = get_tts(ssml, audio_file_name)

    session_attributes['last_state'] = State.TEACH_NEW_WORD
    session_attributes['last_word_id'] = word_id



    return {
        "word": word,
        "responseText": word['es'],
        "audio_uri": audio_uri,
        # "audio_uri": audio_file_name,
        "display": {
            "text": "",
            "images": [
                {
                    "key": word['en'],
                    "uri": "uri"
                }
            ]
        },
        "reward_images": reward_images,
    }

def end_game(event, is_last_answer_correct):
    logger.debug("end game")

    acknowledgement = "Horray! {}".format(ssml_break()) if is_last_answer_correct else "That's okay. {}".format(ssml_break())
    ssml = ssml_speak("{acknowledgement} That's all I'd like to show you today. You've collected so many animals, hope you enjoy your time in our zoo. Have a nice day!".format(acknowledgement=acknowledgement))
    audio_uri = get_tts(ssml, "ending")

    return {
        "word": None,
        "responseText": "ending",
        "audio_uri": audio_uri,
        "display": {
            "text": None,
            "images": [
                {
                    "key": None,
                    "uri": None
                }
            ]
        },
        "feedback": "complete"
    }

def handle_repeat_request(event):
    logger.debug("handle repeat")
    word = get_last_word()
    ssml = ssml_speak(ssml_spanish(word['es']))
    audio_uri = get_tts(ssml, word['en'])

    return {
        "word": word,
        "responseText": word['es'],
        "audio_uri": audio_uri,
        # "audio_uri": word['en'],

        "display": {
            "text": event["parameters"]["repeat-word"],
            "images": [
                {
                    "key": word['en'],
                    "uri": "uri"
                }
            ]
        }
    }

def handle_new_word_answer(event):
    logger.info("handle answer")

    if has_spanish_animal_entity(event) and is_correct_answer(event):
        return handle_correct_new_word_answer(event)
    else:
        return handle_wrong_new_word_answer(event) 


def handle_correct_new_word_answer(event):
    logger.debug("handle correct new answer")

    acknowledgement = get_correct_new_word_answer_acknowledgement(event)
    # url="https://developers.google.com/actions/downloads/ssml/ssml-1.mp3"
    return ask_repeat_word(event, acknowledgement, True)


def handle_wrong_new_word_answer(event):
    logger.debug("handle wrong new word answer")

    word = get_last_word()

    acknowledgement = get_wrong_new_word_answer_acknowledgement(event)
    return ask_repeat_word(event, acknowledgement, False)


def ask_repeat_word(event, acknowledgement, is_last_answer_correct):
    word = get_last_word()
    ssml = ssml_speak('''{acknowledgement} {ssml_break} Now you can say its Spanish name again. {ssml_break} if you speak correctly, the {animal} will react!'''.format(acknowledgement=acknowledgement, ssml_break=ssml_break(), animal=word['en']))

    filename = get_audio_file_name_for_repeat_word(is_last_answer_correct, word['en'])
    audio_uri = get_tts(ssml, filename)

    session_attributes['last_state'] = State.REPEAT_WORD
    session_attributes['last_answer_is_correct'] = is_last_answer_correct

    word_id = get_last_word_id()
    if is_last_answer_correct:
        reward = 2
    else:
        reward = 1

    return {
        "word": word,
        "responseText": "",
        "audio_uri": audio_uri,
        # "audio_uri": filename,
        "display": {
            "text": word['es'],
            "images": [
                {
                    "key": word['en'],
                    "uri": "uri"
                }
            ]
        },
        "reward": reward
    }

def get_audio_file_name_for_teach_new_word(is_last_answer_correct, word):
    audio_file_name = "ack_correct_" if is_last_answer_correct else "ack_wrong_"
    audio_file_name += "teach_word_{}".format(word)
    return audio_file_name

def get_audio_file_name_for_repeat_word(is_last_answer_correct, word):
    audio_file_name = "ack_correct_" if is_last_answer_correct else "ack_wrong_"
    audio_file_name += "ask_repeat_word_{}".format(word)
    return audio_file_name

def has_spanish_animal_entity(event):
    return event['intent'] == "animal-spanish" and event['parameters']['animal-spanish'] is not None

def is_correct_answer(event):
    return event['parameters']['animal-spanish'] == get_last_word()['es']


def handle_repeat_word_answer(event):
    if has_spanish_animal_entity(event) and is_correct_answer(event):
        return handle_correct_repeat_word_answer(event)
    else:
        return handle_wrong_repeat_word_answer(event)

def handle_correct_repeat_word_answer(event):
    acknowledgement = get_correct_repeat_word_answer_acknowledgement(event)
    acknowledgement += '''Let's go see the next one. '''
    
    reward_images = {
        "key": get_last_word()['en'],
        "delay_time": 5500
        }


    word_id = get_last_word_id() + 1

    logger.info("word_id={}, word list length = {}".format(word_id, len(word_list)))
    if word_id >= len(word_list):
        return end_game(event, True)
    else:
        return teach_new_word(event, acknowledgement, True, word_id, reward_images)


def handle_wrong_repeat_word_answer(event):
    acknowledgement = get_wrong_repeat_word_answer_acknowledgement(event)
    word_id = get_last_word_id() + 1

    logger.info("word_id={}, word list length = {}".format(word_id, len(word_list)))
    if word_id >= len(word_list):
        return end_game(event, True)
    return teach_new_word(event, acknowledgement, False, word_id)  # todo: change


def get_correct_new_word_answer_acknowledgement(event):
    word = get_last_word()

    acknowledgement = '''Great! {ssml_break} {teaching_sentence} You got two {animal}s! '''.format(ssml_break=ssml_break(), animal=word['en'], teaching_sentence=get_teaching_sentence(word))

    return acknowledgement


def get_wrong_new_word_answer_acknowledgement(event):
    word = get_last_word()
    acknowledgement = '''Let's try again! {ssml_break}'''.format(word=word['en'], ssml_break=ssml_break())
    return acknowledgement

def get_correct_repeat_word_answer_acknowledgement(event):
    word = get_last_word()
    audio = get_audio_ssml(word['audio_url']) if word['audio_url'] else ""
    logger.debug('''audio: {}'''.format(audio))
    acknowledgement = '''{audio} {ssml_break} Awesome, you're doing a great job! {ssml_break}'''.format(audio=audio, ssml_break=ssml_break(time='500ms'))

    return acknowledgement

def get_wrong_repeat_word_answer_acknowledgement(event):
    return ''' That's okay. Let's go see the next one. '''


    


def get_teaching_sentence(word):
    return 'the {animal} in spanish is <spanish>{spanish}</spanish> {ssml_break}'.format(
        animal=word['en'], spanish=word['es'], ssml_break=ssml_break())

def ssml_speak(sentence):
    return '''<speak><prosody rate="80%">{sentence}</prosody></speak>'''.format(sentence=sentence)


def ssml_spanish(spanish_word):
    return '<spanish>{word}</spanish>'.format(word=spanish_word)

def ssml_break(time='200ms'):
    return '''<break time="{time}"/>'''.format(time=time)


def get_tts(ssml, filename=None):
    # if filename and audio_url_exists(filename):
    #     logger.info("audio url exists for {}".format(filename))
    #     return "https://kids-language-learning.s3.amazonaws.com/sentence/{}.mp3".format(filename)

    print("in get_tts:")
    print("ssml: {}".format(ssml))
    request_url = "http://ec2-54-211-157-190.compute-1.amazonaws.com:5000"
    headers = {'content-type': 'application/json'}
    data = {"ssml": ssml, "filename": filename}
    data = json.dumps(data)
    result = requests.post(url=request_url, data=data,
                           headers=headers, timeout=3000)
    if result is not None:
        return result.json()['audio_uri']
    return None

def audio_url_exists(filename):
    s3 = boto3.client('s3')
    try:
        result = s3.head_object(Bucket='kids-language-learning', Key="sentence/{}.mp3".format(filename))
    except botocore.exceptions.ClientError as e:
        return False

    return True if result else False

def get_audio_ssml(url):
    return '''<audio src="{url}"></audio>'''.format(url=url)


def put_item_to_table(session_id, state=State.START):
    TABLE.put_item(
        Item={
            'session_id': session_id,
            'last_state': state,
        }
    )

def put_session_attribute_to_table():
    TABLE.put_item(
        Item=session_attributes
    )

def get_table_item(session_id):
    response = TABLE.get_item(
        Key={
            'session_id': session_id,
        }
    )
    print("db response: {}".format(response))
    try:
        item = response['Item']
        logger.info('get table item: {}'.format(item))
    except KeyError as e:
        logger.info("Session doesn't exists. {}".format(e))
        return None

    return item


def print_table_log(table):
    logger.info('table: {}'.format(table))

# ======class=====

