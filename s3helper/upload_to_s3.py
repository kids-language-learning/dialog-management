import logging
import boto3
from botocore.exceptions import ClientError

bucket_name = 'kids-language-learning'

def upload_file(file_name: str, bucket: str, object_name=None):
    return __upload('upload_file', file_name, bucket, object_name)

def upload_fileobj(fileobj_name:str, bucket: str, object_name=None):
    return __upload('upload_fileobj', fileobj_name, bucket, object_name)

def __upload(func_name: str, file_name: str, bucket: str, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = getattr(s3_client, func_name)(
            file_name, 
            bucket, 
            object_name, 
            ExtraArgs={'ACL': 'public-read'})
    except ClientError as e:
        logging.error(e)
        return False
    return True

def get_bucket_url(bucket_name: str, key: str) -> str:
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)

    s3_client = boto3.client('s3')
    location = s3_client.get_bucket_location(Bucket='kids-language-learning')

    return "https://{bucket_name}.s3.amazonaws.com/{key}".format(bucket_name=bucket_name, key=key)


if __name__ == "__main__":
    upload_file('output/output-polly.mp3', 'kids-language-learning', 'output-polly4.mp3')    
    # url = get_bucket_url('kids-language-learning', 'output-polly.mp3')
    # print(url)