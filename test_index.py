from index import *


def test_get_tts():
    result = get_tts("<speak>I can talk to you<spanish>la manzana</spanish></speak>")
    assert result == '1'


def test_handler():
    event = {
        "intent": "fruit-spanish",
        "parameters": {
            "fruit-spanish": "la manzana"
        }
    }
    result = handler(event, None)
    assert result == ''


def test_dynamodb_put_item():
    put_item_to_table()


def test_state():
    print("state: {}".format(State.START))


def test_update_session_attributes():
    update_session_attributes('12345')
    print("session attributes: {}".format(session_attributes))
    print("Table: {}".format(TABLE))
